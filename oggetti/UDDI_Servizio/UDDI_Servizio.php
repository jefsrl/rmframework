<?php
/**
 * Servizio caricato nel registro UDDI
 * 
 * @version 1.1.0 28/01/2015 11.48
 * @author Andrea Vallorani
 */
class UDDI_Servizio extends CB_Oggetto{

    protected $_contenitore = 'uddi_services';
    protected $_campi = array(
            'id' => array('K'=>1,'type'=>'Counter','l'=>10),
            'uuid' => array('l'=>255,'bu'=>1,'bi'=>1),
            'uuidB' => array('l'=>255,'bu'=>1,'bi'=>1),
            'uuidTP' => array('l'=>255,'bu'=>1,'bi'=>1),
            'uuidTB' => array('l'=>255,'bu'=>1,'bi'=>1),
            'ente' => array('r'=>1,'type'=>'Link','link'=>'UDDI_Ente'),
            'tipo' => array('r'=>1,'l'=>1,'type'=>'Enum','enum'=>array(1,2,3,4,5)),
            'nome' => array('r'=>1,'l'=>100),
            'descrizione' => array('type'=>'Text'),
            'endpoint' => array('r'=>1,'l'=>250),
            'wsdl' => array('l'=>250),
            'autore' => array('l'=>50,'type'=>'Link','link'=>'CB_Credenziali','bu'=>1,'bi'=>1),
            'dataIns' => array('type'=>'DataTime','bi'=>1,'bu'=>1),
            'dataMod' => array('type'=>'DataTime','null'=>1,'bi'=>1,'bu'=>1),
            'sync' => array('type'=>'DataTime','null'=>1,'bu'=>1,'bi'=>1),
            'sync_err' => array('null'=>1,'bu'=>1,'bi'=>1),
            'block' => array('type'=>'DataTime','null'=>1,'bu'=>1,'bi'=>1)
    );

    //configurazione
    const PARAMETRO = 'service';
    const TITOLO_ARCHIVIO = 'Servizi UDDI';
    protected $campiArchivio = array('id','ente','nome','tipo','endpoint','autore','dataIns');
    protected $colonneArchivio = array('autore'=>'60px','dataIns'=>'82px','endpoint'=>'250px','tipo'=>'85px','ente'=>'110px');
    
    function _beforeInsert(){
        if($this->tipo<=2 && !$this->wsdl) throw new Exception('Per un Web Service il campo wsdl � obbligatorio');
        $this->set('dataIns',$this->_dataOra());
        $this->set('autore',User::get('name'));
        $this->set('uuid',AV_Generator::uuidV4());
        $this->set('uuidB',AV_Generator::uuidV4());
        $this->set('uuidTP',AV_Generator::uuidV4());
        $this->set('uuidTB',AV_Generator::uuidV4());
    }
    
    function _afterInsert(){
        $this->sync();
    }
    
    function _beforeUpdate(){
        if(!$this->upsync) $this->set('dataMod',$this->_dataOra());
    }
    
    function _afterUpdate(){
        if(!$this->upsync) $this->sync();
    }
    
    function _beforeDelete(){
        $this->removeFromUddi();
    }
    
    function block(){
        $this->removeFromUddi();
        parent::block();
    }
    
    function unblock(){
        $this->sync();
        parent::unblock();
    }

    function getTableSql(){
        return "CREATE TABLE IF NOT EXISTS `$this->_contenitore` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(36) NOT NULL,
  `uuidB` varchar(36) NOT NULL,
  `uuidTP` varchar(36) NOT NULL,
  `uuidTB` varchar(36) NOT NULL,
  `ente` int(10) unsigned NOT NULL,
  `tipo` tinyint unsigned NOT NULL 
  `nome` varchar(100) NOT NULL,
  `descrizione` tinytext,
  `endpoint` varchar(250) NOT NULL,
  `wsdl` varchar(250) NOT NULL,
  `autore` varchar(50) NOT NULL,
  `dataIns` datetime NOT NULL,
  `dataMod` datetime DEFAULT NULL,
  `sync` datetime DEFAULT NULL,
  `sync_err ` tinytext,
  `block` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uuid` (`uuid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";
    }
    
    /**
     * TRUE se il servizio � un WebService, FALSE altrimenti
     * @return boolean
     */
    function isWS(){
        return ($this->tipo<=2);
    }
    
    /**
     * Sincronizza il servizio con il catalogo UDDI
     */
    function sync(){
        try{
            switch($this->tipo){
                case 1: 
                case 2: $bdesc='SOAP binding'; break;
                case 3: $bdesc=($this->wsdl) ?:'File'; break;
                case 4: $bdesc='Pagina web'; break;
            }
            $client = new AV_SoapClient(Config::get('uddi_url').'services/publish?wsdl');
            if($this->tipo<=2){
                $dom = DOMDocument::load(trim($this->wsdl));
                if(!$dom) throw new Exception('Impossibile caricare la seguente WSDL: '.$this->wsdl);
                $servicename=$dom->getElementsByTagName('service')->item(0)->getAttribute('name');
                $portname=$dom->getElementsByTagName('portType')->item(0)->getAttribute('name');
                $bindingname=$dom->getElementsByTagName('binding')->item(0)->getAttribute('name');
                $namespace=$dom->documentElement->getAttribute('targetNamespace');
                $spcoop = ($this->tipo==2) ? '<ns1:keyedReference tModelKey="uddi:uddi.org:wsdl:categorization:protocol" keyName="SPCoop protocol" keyValue="uddi:uddi.org:protocol:soap" />' : '';
                $client->save_tModel(new SoapVar('<ns1:save_tModel>
                    <ns1:authInfo>'.Uddi::getAuthToken().'</ns1:authInfo>
                    <ns1:tModel tModelKey="'.$this->uuidTP.'">
                        <ns1:name xml:lang="it">'.$portname.'</ns1:name>
                        <ns1:overviewDoc>
                           <ns1:overviewURL useType="wsdlInterface">'.$this->wsdl.'</ns1:overviewURL>
                        </ns1:overviewDoc>
                        <ns1:categoryBag>
                           <ns1:keyedReference tModelKey="uddi:uddi.org:xml:namespace" keyName="portType namespace" keyValue="'.$namespace.'"/>
                           <ns1:keyedReference tModelKey="uddi:uddi.org:wsdl:types" keyName="WSDL type" keyValue="portType"/>
                        </ns1:categoryBag>
                    </ns1:tModel>
                    <ns1:tModel tModelKey="'.$this->uuidTB.'">
                        <ns1:name xml:lang="it">'.$bindingname.'</ns1:name>
                        <ns1:overviewDoc>
                           <ns1:overviewURL useType="wsdlInterface">'.$this->wsdl.'</ns1:overviewURL>
                        </ns1:overviewDoc>
                        <ns1:categoryBag>
                           <ns1:keyedReference tModelKey="uddi:uddi.org:xml:namespace" keyName="binding namespace" keyValue="'.$namespace.'"/>
                           <ns1:keyedReference tModelKey="uddi:uddi.org:wsdl:types" keyName="WSDL type" keyValue="binding"/>
                           <ns1:keyedReference tModelKey="uddi:uddi.org:categorization:types" keyName="uddi-org:types" keyValue="wsdlSpec"/>
                           <ns1:keyedReference tModelKey="uddi:uddi.org:wsdl:porttypereference" keyName="portType reference" keyValue="'.$this->uuidTP.'"/>
                           <ns1:keyedReference tModelKey="uddi:uddi.org:wsdl:categorization:protocol" keyName="SOAP protocol" keyValue="uddi:uddi.org:protocol:soap" />
                           '.$spcoop.'
                           <ns1:keyedReference tModelKey="uddi:uddi.org:wsdl:categorization:transportport" keyName="HTTP protocol" keyValue="uddi:uddi.org:protocol:http" />
                        </ns1:categoryBag>
                    </ns1:tModel>
                  </ns1:save_tModel>',XSD_ANYXML));

                $client->save_service(new SoapVar('<ns1:save_service>
                    <ns1:authInfo>'.Uddi::getAuthToken().'</ns1:authInfo>
                    <ns1:businessService serviceKey="'.$this->uuid.'" businessKey="'.$this->obj('ente')->uuid.'">
                        <ns1:name xml:lang="it">'.$this->nome.'</ns1:name>
                        <ns1:description xml:lang="it">'.$this->descrizione.'</ns1:description>
                        <ns1:bindingTemplates>
                           <ns1:bindingTemplate bindingKey="'.$this->uuidB.'" serviceKey="'.$this->uuid.'">
                              <ns1:description xml:lang="it">'.$bdesc.'</ns1:description>
                              <ns1:accessPoint useType="endPoint">'.$this->endpoint.'</ns1:accessPoint>
                              <ns1:tModelInstanceDetails>
                                 <ns1:tModelInstanceInfo tModelKey="'.$this->uuidTB.'">
                                    <ns1:description xml:lang="en">
                                        The wsdl:binding that this wsdl:port implements. The instanceParms specifies the port local name.
                                    </ns1:description>
                                    <ns1:instanceDetails>
                                       <ns1:instanceParms>'.$portname.'</ns1:instanceParms>
                                   </ns1:instanceDetails>
                                 </ns1:tModelInstanceInfo>
                                 <ns1:tModelInstanceInfo tModelKey="'.$this->uuidTP.'">
                                    <ns1:description xml:lang="en">
                                        The wsdl:portType that this wsdl:port implements
                                    </ns1:description>
                                 </ns1:tModelInstanceInfo>
                              </ns1:tModelInstanceDetails>
                           </ns1:bindingTemplate>
                        </ns1:bindingTemplates>
                        <ns1:categoryBag>
                            <ns1:keyedReference tModelKey="uddi:uddi.org:xml:namespace" keyName="service namespace" keyValue="'.$namespace.'"/>
                            <ns1:keyedReference tModelKey="uddi:uddi.org:xml:localname" keyName="service local name" keyValue="'.$servicename.'"/>
                            <ns1:keyedReference tModelKey="uddi:uddi.org:wsdl:types" keyName="WSDL type" keyValue="service"/>
                     </ns1:categoryBag>
                  </ns1:businessService>
                </ns1:save_service>',XSD_ANYXML));
            }
            else{
                $client->save_service(new SoapVar('<ns1:save_service>
                    <ns1:authInfo>'.Uddi::getAuthToken().'</ns1:authInfo>
                    <ns1:businessService serviceKey="'.$this->uuid.'" businessKey="'.$this->obj('ente')->uuid.'">
                        <ns1:name xml:lang="it">'.$this->nome.'</ns1:name>
                        <ns1:description xml:lang="it">'.$this->descrizione.'</ns1:description>
                        <ns1:bindingTemplates>
                           <ns1:bindingTemplate bindingKey="'.$this->uuidB.'" serviceKey="'.$this->uuid.'">
                              <ns1:description xml:lang="it">'.$bdesc.'</ns1:description>
                              <ns1:accessPoint useType="endPoint">'.$this->endpoint.'</ns1:accessPoint>
                              <ns1:tModelInstanceDetails>
                                  <ns1:tModelInstanceInfo tModelKey="uddi:uddi.org:transport:http"> </ns1:tModelInstanceInfo>
                              </ns1:tModelInstanceDetails>
                           </ns1:bindingTemplate>
                        </ns1:bindingTemplates>
                  </ns1:b   usinessService>
                </ns1:save_service>',XSD_ANYXML));
            }
            $this->set('sync',$this->_dataOra());
            $this->set('sync_err','');
            $this->set('block','');
        }
        catch(Exception $e){
            $this->set('sync_err',$e->getMessage());
            $this->set('block',$this->_dataOra());
        }
        $this->upsync=true;
        $this->update();
    }
    
    /**
     * Rimuove il servizio dal catalogo UDDI
     */
    function removeFromUddi(){
        $client = new AV_SoapClient(Config::get('uddi_url').'services/publish?wsdl');

        //cancello servizio
        $client->delete_service(new SoapVar('<ns1:delete_service>
         <ns1:authInfo>'.Uddi::getAuthToken().'</ns1:authInfo>
         <ns1:serviceKey>'.$this->uuid.'</ns1:serviceKey>
      </ns1:delete_service>',XSD_ANYXML));

        //cancello binding
        $client->delete_binding(new SoapVar('<ns1:delete_binding>
         <ns1:authInfo>'.Uddi::getAuthToken().'</ns1:authInfo>
         <ns1:bindingKey>'.$this->uuidB.'</ns1:bindingKey>
      </ns1:delete_binding>',XSD_ANYXML));

        if($this->tipo<=2){
            //cancello tmodels
            $client->delete_tModel(new SoapVar('<ns1:delete_tModel>
             <ns1:authInfo>'.Uddi::getAuthToken().'</ns1:authInfo>
             <ns1:tModelKey>'.$this->uuidTB.'</ns1:tModelKey>
             <ns1:tModelKey>'.$this->uuidTP.'</ns1:tModelKey>
          </ns1:delete_tModel>',XSD_ANYXML));
        }
    }
    
    function view(){
        if($_GET['resync']) $this->sync();
        if($this->sync_err) $this->sync_err='<span style="color:red">'.$this->sync_err.'</span>';
        $this->sync=$this->obj('sync')->gets('d/m/Y H:i').' <small>(<a href="'.CMS::modUrl(array('resync'=>1)).'">Sincronizza</a>)</small>';
        $this->_campi['sync']['type']='';
        return parent::view();
    }
    
}
