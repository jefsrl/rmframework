<?php
$archivio['titolo']='Servizi UDDI';

$traduzione['id'] = 'ID';
$traduzione['uuid'] = 'UUID Servizio';
$traduzione['uuidB'] = 'UUID BindingTemplate';
$traduzione['uuidTP'] = 'UUID TModel Port';
$traduzione['uuidTB'] = 'UUID TModel Binding';
$traduzione['tipo'] = 'Tipo';
$traduzione['tipo_enum'][1] = 'Web Service';
$traduzione['tipo_enum'][2] = 'Web Service su PdD';
$traduzione['tipo_enum'][3] = 'REST Service';
$traduzione['tipo_enum'][4] = 'File';
$traduzione['tipo_enum'][5] = 'Pagina web';
$traduzione['nome'] = 'Nome';
$traduzione['ente'] = 'Ente';
$traduzione['descrizione'] = 'Descrizione';
$traduzione['dataIns'] = 'Data ins.';
$traduzione['dataMod'] = 'Data mod.';
$traduzione['autore'] = 'Autore';
$traduzione['endpoint'] = 'Accesspoint';
$traduzione['wsdl'] = 'Wsdl';
$traduzione['sync'] = 'Sinc. con UDDI il';
$traduzione['sync_err'] = 'Errore di sincronizzazione';