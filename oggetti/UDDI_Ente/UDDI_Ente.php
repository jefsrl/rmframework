<?php
/**
 * Servizio caricato nel registro UDDI
 * 
 * @version 1.0.0 13/11/2013 13.22
 * @author Andrea Vallorani
 */
class UDDI_Ente extends CB_Oggetto{

    protected $_contenitore = 'uddi_entities';
    protected $_campi = array(
            'id' => array('K'=>1,'type'=>'Counter','l'=>10),
            'uuid' => array('l'=>255,'bi'=>1,'bu'=>1),
            'nome' => array('r'=>1,'l'=>100),
            'categorie' => array('l'=>200,'null'=>1,'type'=>'Tags'),
            'pag' => array('l'=>10,'null'=>1,'type'=>'Link','link'=>'CB_File'),
            'autore' => array('l'=>50,'type'=>'Link','link'=>'CB_Credenziali','bu'=>1,'bi'=>1),
            'dataIns' => array('type'=>'DataTime','bi'=>1,'bu'=>1),
            'dataMod' => array('type'=>'DataTime','null'=>1,'bi'=>1,'bu'=>1),
            'sync' => array('type'=>'DataTime','null'=>1,'bu'=>1,'bi'=>1),
            'sync_err' => array('null'=>1,'bu'=>1,'bi'=>1),
            'block' => array('type'=>'DataTime','null'=>1,'bu'=>1,'bi'=>1),
            'istat' => array('null'=>1)
    );

    //configurazione
    const PARAMETRO = 'entity';
    const TITOLO_ARCHIVIO = 'Enti UDDI';
    protected $campiArchivio = array('id','nome','categorie','autore','dataIns','sync');
    protected $colonneArchivio = array('autore'=>'60px','dataIns'=>'70px');
    
    function _beforeInsert(){
        $this->set('dataIns',$this->_dataOra());
        $this->set('autore',User::get('name'));
        $this->set('uuid',AV_Generator::uuidV4());
    }
    
    function _afterInsert(){
        $this->sync();
    }
    
    function _beforeUpdate(){
        if(!$this->upsync) $this->set('dataMod',$this->_dataOra());
    }
    
    function _afterUpdate(){
        if(!$this->upsync) $this->sync();
    }
    
    function _beforeDelete(){
        $this->removeFromUddi();
    }
    
    function block(){
        $this->removeFromUddi();
        parent::block();
    }
    
    function unblock(){
        $this->sync();
        parent::unblock();
    }

    function getTableSql(){
        return "CREATE TABLE IF NOT EXISTS `$this->_contenitore` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(36) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `istat` varchar(6) DEFAULT NULL,
  `categorie` varchar(200) DEFAULT NULL,
  `pag` int(10) unsigned DEFAULT NULL,
  `autore` varchar(50) NOT NULL,
  `dataIns` datetime NOT NULL,
  `dataMod` datetime DEFAULT NULL,
  `sync` datetime DEFAULT NULL,
  `sync_err ` tinytext,
  `block` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uuid` (`uuid`),
  UNIQUE KEY `pag` (`pag`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";
    }
    
    function sync(){
        try{
            $client = new AV_SoapClient(Config::get('uddi_url').'services/publish?wsdl');
            $client->save_business(new SoapVar('<ns1:save_business>
             <ns1:authInfo>'.Uddi::getAuthToken().'</ns1:authInfo>
             <ns1:businessEntity businessKey="'.$this->uuid.'">
                <ns1:name xml:lang="it">'.$this->nome.'</ns1:name>
                <ns1:description xml:lang="it">'.implode(',',$this->categorie).'</ns1:description>
             </ns1:businessEntity>
          </ns1:save_business>',XSD_ANYXML));
            $this->set('sync',$this->_dataOra());
            $this->set('sync_err','');
            $this->set('block','');
        }
        catch(Exception $e){
            $this->set('sync_err',$e->getMessage());
            $this->set('block',$this->_dataOra());
        }
        $this->upsync=true;
        $this->update();
    }
    
    function removeFromUddi(){
        try{
            $client = new AV_SoapClient(Config::get('uddi_url').'services/publish?wsdl');
            $client->delete_business(new SoapVar('<ns1:delete_business>
             <ns1:authInfo>'.Uddi::getAuthToken().'</ns1:authInfo>
             <ns1:businessKey>'.$this->uuid.'</ns1:businessKey>
          </ns1:delete_business>',XSD_ANYXML));
        }
        catch(Exception $e){
            
        }
    }
    
    function view(){
        if($_GET['resync']) $this->sync();
        if($this->sync_err) $this->sync_err='<span style="color:red">'.$this->sync_err.'</span> <small>(<a href="'.CMS::modUrl(array('resync'=>1)).'">Sincronizza</a>)</small>';
        return parent::view();
    }
    
}
