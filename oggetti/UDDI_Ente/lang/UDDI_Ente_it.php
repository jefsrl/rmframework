<?php
$archivio['titolo']='Enti UDDI';

$traduzione['id'] = 'ID';
$traduzione['nome'] = 'Nome';
$traduzione['istat'] = 'Cod. Istat';
$traduzione['uuid'] = 'UUID';
$traduzione['sync'] = 'Sinc. con UDDI il';
$traduzione['sync_err'] = 'Errore di sincronizzazione';
$traduzione['block'] = 'Bloccato';
$traduzione['dataIns'] = 'Data ins.';
$traduzione['dataMod'] = 'Data mod.';
$traduzione['autore'] = 'Autore';
$traduzione['categorie'] = 'Categorie';
$traduzione['pag'] = 'Pagina web';
