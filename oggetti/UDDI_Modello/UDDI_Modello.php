<?php
/**
 * Modello di richiesta/risposta da utilizzare nei blocchi UDDI_WSClient
 * @version 1.0.1 10/11/15 12.27
 * @author Andrea Vallorani
 */
class UDDI_Modello extends CB_Oggetto{

    protected $_contenitore = 'uddi_models';
    protected $_campi = array(
            'id' => array('K'=>1,'type'=>'Counter','l'=>10),
            'nome' => array('r'=>1,'l'=>100),
            'response' => array('r'=>1,'type'=>'Text','col'=>20),
            'request' => array('type'=>'Text','col'=>10),
            'autore' => array('l'=>50,'type'=>'Link','link'=>'CB_Credenziali','bu'=>1,'bi'=>1),
            'dataIns' => array('type'=>'DataTime','bi'=>1,'bu'=>1),
            'dataMod' => array('type'=>'DataTime','null'=>1,'bi'=>1,'bu'=>1),
            'block' => array('type'=>'DataTime','null'=>1,'bu'=>1,'bi'=>1)
    );

    //configurazione
    const PARAMETRO = 'model';
    const TITOLO_ARCHIVIO = 'Modelli WS';
    protected $campiArchivio = array('id','nome','autore','dataIns');
    protected $colonneArchivio = array('autore'=>'80px','dataIns'=>'80px');
    
    function _beforeInsert(){
        $this->set('dataIns',$this->_dataOra());
        $this->set('autore',User::get('name'));
    }
    
    function _afterInsert(){}
    
    function _beforeUpdate(){
        $this->set('dataMod',$this->_dataOra());
    }

    function getTableSql(){
        return "CREATE TABLE IF NOT EXISTS `$this->_contenitore` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) NOT NULL,
  `request` text,
  `response` text,
  `autore` varchar(50) NOT NULL,
  `dataIns` datetime NOT NULL,
  `dataMod` datetime DEFAULT NULL,
  `block` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;";
    }
    
}