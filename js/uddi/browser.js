$(function(){
    $('#info,#tree').css('height',$(document).height()-68);
    $("#tree").jstree({
        "plugins":["themes","html_data","ui"],
        themes:{ theme:"classic"}
    }).bind("select_node.jstree", function (event, data){
        var a=data.rslt.obj.children('a');
        $.get(a.attr('href'),function(code){
            $('#info').html('<textarea>'+code+'</textarea>'); 
            var editor=CodeMirror.fromTextArea($('#info').children('textarea').get(0),{lineWrapping:false,mode:"xml"});
            editor.setSize("100%",$('#info').height()-10);
            if(a.is('[rel="s"]') && !a.next().is('ul')){
                var xmlDoc = $.parseXML(code);
                $(xmlDoc).find('ns2\\:bindingTemplate').each(function(){
                    var name=$(this).find('ns2\\:accessPoint').html();
                    var url=binding+'&k='+$(this).attr('bindingKey');
                    var newnode =  $("#tree").jstree("create_node",data.rslt.obj,"first",{attr:{id:'a0'},data:{title:name,attr:{rel:'a',href:url}}},function(){},true);
                    $(this).find('ns2\\:tModelInstanceInfo').each(function(i,j){
                        name=$(this).find('ns2\\:description').html();
                        url=tmodel+'&k='+$(this).attr('tModelKey');
                        $("#tree").jstree("create_node",newnode,"first",{attr:{id:'t'+i},data:{title:name,attr:{rel:'a',href:url}}},function(){},true);
                    });
                    $("#tree").jstree("open_node",data.rslt.obj);
                });
                
            }
        });
    });
});