<?php
/** 
 * Controller estensione Uddi
 * 
 * @version 1.0.1 28/01/2015 12.50
 * @author Andrea Vallorani
 * 
 * @link https://www.oasis-open.org/committees/uddi-spec/doc/tn/uddi-spec-tc-tn-wsdl-v2.htm WSDL to UDDI spec
 * @link http://kasunweranga.blogspot.it/2011/04/wsdl-to-uddi-mapping.html WSDL to UDDI tutorial
 * @link http://testuddi.regione.marche.it/uddi/web UDDI Regione Marche
 */
class Uddi extends AV_AppController{
    
    protected $registry = array('browser'=>'',
                                'businessInfo'=>'k',
                                'serviceInfo'=>'k',
                                'bindingInfo'=>'k',
                                'tmodelInfo'=>'k',
                                'stampa'=>'name');
    
    static private $authToken = '';
    
    public function stampa($nome){
        require 'lib/tcpdf/tcpdf.php';
        $html=User::space('WSClient_out');
        $qrcode1='<table width="30%"><tr><td><img src="https://chart.googleapis.com/chart?chs=80x80&cht=qr&chl='.urlencode($_SERVER['HTTP_REFERER']).'&chld=L|1&choe=UTF-8"></td><td><img src="https://chart.googleapis.com/chart?chs=80x80&cht=qr&chl='.urlencode("Paolo|Rossi|RNLLSE19L62H501S").'&chld=L|1&choe=UTF-8"></td></tr></table>';
        
        $html.=$qrcode1;
        $path='public/tmp/ws'.time().'.pdf';
        
        $pdf=new TCPDF(PDF_PAGE_ORIENTATION,PDF_UNIT,PDF_PAGE_FORMAT,true,'UTF-8',false);
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetHeaderData('','','PortaleRaffaello.it');
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        $pdf->setLanguageArray($l);
        $pdf->SetFont('helvetica','',10);
        $pdf->AddPage();
        $pdf->writeHTML($html,true,false,true,false,'');
        $pdf->lastPage();
       
        file_put_contents($path,$pdf->Output('','S')); 
        AV_HTTPClient::download($nome,$path,'application/pdf','inline');
    }
    
    public function browser(){
        $p=new AV_PaginaWeb;
        $p->addCSS('css/sys/zero.css');
        $p->addCSS('css/sys/uddibrowser.css');
        $p->addJS(CMS::jquery);
        $p->addJS('js/jstree/jquery.jstree.js');
        $p->addJS('js/uddi/browser.js');
        $p->addJSCode('$.jstree._themes = "'.CMS::siteDir().'/js/jstree/";'
            . 'var tmodel = "'.CMS::newUrl('uddi','tmodelInfo','k','','normal').'";'
            . 'var binding = "'.CMS::newUrl('uddi','bindingInfo','k','','normal').'";');
        Pages::addPlugin('codemirror','',$p);
        $p->add($h=new AV_Tag('header'));
        $h->add('<h1>UDDI web browser</h1>('.Config::get('uddi_url').')');
        $p->add($content=new AV_DivId('content'));
        $content->add($stats=new AV_DivId('stats'));
        try{
            $client = new AV_SoapClient(Config::get('uddi_url').'services/inquiry?wsdl');
            $client->__enableTrace();
            $rows=$client->find_business(new SoapVar('<ns1:find_business maxRows="1000">
         <ns1:findQualifiers>
            <ns1:findQualifier>uddi:uddi.org:findqualifier:approximatematch</ns1:findQualifier>
            <ns1:findQualifier>uddi:uddi.org:findqualifier:caseinsensitivematch</ns1:findQualifier>
         </ns1:findQualifiers>
         <ns1:name xml:lang="it">%</ns1:name>
      </ns1:find_business>',XSD_ANYXML));
            $enti=(is_array($rows->businessInfos->businessInfo)) ? $rows->businessInfos->businessInfo : $rows->businessInfos;
            $content->add($tree=new AV_DivId('tree'));
            $content->add(new AV_DivId('info'));
            $tree->add($menu=new AV_List());
            $ne=$ns=0;
            foreach($enti as $row){
                $servizi=(is_array($row->serviceInfos->serviceInfo)) ? $row->serviceInfos->serviceInfo : $row->serviceInfos;
                $elenco=array();
                foreach($servizi as $v){
                    $elenco[$v->name->_]=new AV_Link($v->name->_,CMS::newUrl('uddi','serviceInfo',$v->serviceKey,'','normal'),array('rel'=>'s'));
                    $ns++;
                }
                asort($elenco);
                $menu->add(array(new AV_Link($row->name->_,CMS::newUrl('uddi','businessInfo',$row->businessKey,'','normal'),array('rel'=>'b')),new AV_List($elenco)));
                $ne++;
            }
            //$p->add(var_export($rows->businessInfos->businessInfo,1));
            $stats->add("Enti $ne, Servizi $ns");
            $p->add($client->__getLastRequest());
        }
        catch(Exception $e){
            $content->add($e->getMessage());
        }
        return $p;
    }
    
    public function businessInfo($key){
        try{
            $client = new AV_SoapClient(Config::get('uddi_url').'services/inquiry?wsdl');
            $client->__enableTrace();
            $client->get_businessDetail(new SoapVar('<ns1:get_businessDetail>
         <ns1:businessKey>'.$key.'</ns1:businessKey>
      </ns1:get_businessDetail>',XSD_ANYXML));
            $doc = new DomDocument('1.0');
            $doc->loadXML($client->__getLastResponse());
            $doc->preserveWhiteSpace = false;
            $doc->formatOutput = true;
            return $doc->saveXML();
        }
        catch(Exception $e){
            return $e->getMessage();
        }
    }
    
    public function serviceInfo($key){
        try{
            $client = new AV_SoapClient(Config::get('uddi_url').'services/inquiry?wsdl');
            $client->__enableTrace();
            $client->get_serviceDetail(new SoapVar('<ns1:get_serviceDetail>
         <ns1:serviceKey>'.$key.'</ns1:serviceKey>
      </ns1:get_serviceDetail>',XSD_ANYXML));
            $doc = new DomDocument('1.0');
            $doc->loadXML($client->__getLastResponse());
            $doc->preserveWhiteSpace = false;
            $doc->formatOutput = true;
            return $doc->saveXML();
        }
        catch(Exception $e){
            return $e->getMessage();
        }
    }
    
    public function bindingInfo($key){
        try{
            $client = new AV_SoapClient(Config::get('uddi_url').'services/inquiry?wsdl');
            $client->__enableTrace();
            $client->get_bindingDetail(new SoapVar('<ns1:get_bindingDetail>
         <ns1:bindingKey>'.$key.'</ns1:bindingKey>
      </ns1:get_bindingDetail>',XSD_ANYXML));
            $doc = new DomDocument('1.0');
            $doc->loadXML($client->__getLastResponse());
            $doc->preserveWhiteSpace = false;
            $doc->formatOutput = true;
            return $doc->saveXML();
        }
        catch(Exception $e){
            return $e->getMessage();
        }
    }
    
    public function tmodelInfo($key){
        try{
            $client = new AV_SoapClient(Config::get('uddi_url').'services/inquiry?wsdl');
            $client->__enableTrace();
            $client->get_tModelDetail(new SoapVar('<ns1:get_tModelDetail>
         <ns1:tModelKey>'.$key.'</ns1:tModelKey>
      </ns1:get_tModelDetail>',XSD_ANYXML));
            $doc = new DomDocument('1.0');
            $doc->loadXML($client->__getLastResponse());
            $doc->preserveWhiteSpace = false;
            $doc->formatOutput = true;
            return $doc->saveXML();
        }
        catch(Exception $e){
            return $e->getMessage();
        }
    }
    
    static function getAuthToken(){
        if(!self::$authToken){
            try{
                $client = new AV_SoapClient(Config::get('uddi_url').'services/security?wsdl');
                $resp=$client->get_authToken(new SoapVar('<ns1:get_authToken userID="'.Config::get('uddi_usr').'" cred="'.Config::get('uddi_pwd').'"/>',XSD_ANYXML));
                Log::add($resp);
                self::$authToken = $resp->authInfo;
            }
            catch(Exception $e){
                self::$authToken = '';
            }
        }
        return self::$authToken;
    }
}