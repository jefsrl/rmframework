<?php
/***************************************************
                    LOGIN COHESION  
 v: 2.1.0
 d: 04/06/2013 16.16
 c: Tecut_CohesionLogin 
 g: Pubblica amministrazione
 t: Blocco per inserire l'autenticazione tramite cohesion nel sito
 a: Andrea Vallorani
****************************************************/

class Tecut_CohesionLogin extends CB_Block{
 
    protected $campiConfig  = array('ruolo','mask','version');
    protected $campiDesc = array('mask'=>'Se impostato a SI, l\'utente che entra nella pagina viene subito reindirizzato al login di Cohesion');

    public function inizializza(){
        //
    }

    protected function costruisciBlocco(){
        if(!$this->config['ruolo']){
            if($this->inModifica) $this->body->add('Bisogna configurare il parametro ruolo per attivare il blocco');
            return;
        }
        if(!User::isLogin()){
            if(!$this->config['mask'] && !$this->param('check')){
                $t=$this->getTemplate();
                $tpl=new AV_Template($t['non_loggato']);
                $tpl->field('URL_LOGIN',CMS::modUrl(array($this->sigla.'check'=>1)));
                $tpl->field('CMS_DIR',CMS::siteDir());
                $this->body->add($tpl->out());
                return;
            }
            else{
                try{
                    User::open();
                    if(!$this->config['version']){
                        require_once 'tools/Tecut_CohesionLogin/TecutWSClient.php';
                        $domain=CMS::getAddress();
                        $returnUrl=AV_HTTPClient::getServerDomain().CMS::newUrl($_GET,'','','','normal');
                        require_once 'tools/Tecut_CohesionLogin/Cohesion.class.php';
                        $cohesion=new Cohesion($domain,$returnUrl);
                    }
                    else{
                        require_once 'tools/Tecut_CohesionLogin/Cohesion2.class.php';
                        $cohesion=new Cohesion2;
                        $cohesion->useSSO(false);
                    }
                    $cohesion->auth();
                    $cittadino = AV_Cittadino::first(array('altro'=>'cohesion:'.$cohesion->username));
                    if(!$cittadino){
                        try{
                            $profilo = $cohesion->profile;
                            $cittadino = new AV_Cittadino;
                            $cittadino->set('cognome',$profilo['cognome']);
                            $cittadino->set('nome',$profilo['nome']);
                            $cittadino->set('sesso',$profilo['sesso']);
                            $cittadino->set('altro','cohesion:'.$cohesion->username);
                            $cittadino->set('codiceFiscale',$profilo['codice_fiscale']);
                            $cittadino->set('comuneResidenza',$profilo['localita_residenza']);
                            $cittadino->insert();

                            $nuovoUtente = new CB_Credenziali();
                            $nuovoUtente->set('username',$cohesion->username.'@cohesion');
                            $nuovoUtente->set('password',AV_Generator::password(8));
                            $nuovoUtente->set('email',$profilo['email'] ? $profilo['email'] : 'info@jef.it');
                            $nuovoUtente->set('stato','on');
                            $nuovoUtente->set('ruolo',$this->config['ruolo']);
                            $nuovoUtente->set('profilo',$cittadino->id);
                            $nuovoUtente->set('primoAccesso',$nuovoUtente->_dataOra());
                            $nuovoUtente->set('autore',$cohesion->username.'@cohesion');
                            $nuovoUtente->insert();
                        }
                        catch(AV_Exception $e){
                            //
                        }
                    }
                    User::set($cohesion->username,array(
                       'ruolo'=>$this->config['ruolo'],
                       'email'=>$credenziali->email,
                       'profilo'=>$cittadino->id
                    ));
                    User::space('cohesion_sso',$cohesion->id_sso);
                    User::space('cohesion_aspnet',$cohesion->id_aspnet);
                    $utente = new CB_Credenziali($cohesion->username.'@cohesion');
                    $utente->set('ultimoAccesso',$utente->_dataOra());
                    $utente->set('numeroAccessi',$utente->numeroAccessi+1);
                    $utente->update();
                    CMS::redirect(CMS::modUrl('',array('auth',$this->sigla.'check')));
                    exit;
                }
                catch(Exception $e){
                    die('Errore nella procedura di autenticazione: '.$e->getMessage());
                }
            }
        }
        else{
            $t=$this->getTemplate();
            $tpl=new AV_Template($t['loggato']);
            $tpl->field('URL_LOGOUT',CMS::newUrl('login','out',Pages::$file->id));
            $tpl->field('CB_Credenziali',new CB_Credenziali(User::get('name')));
            $tpl->field('AV_Cittadino',new AV_Cittadino(User::get('profilo')));
            $this->body->add($tpl->out());
            return;
        }
    }
    protected function formImpostazioni($form){
        $ruoli = AV_Find::getElenco('CB_Ruolo', 'id', 'nome','-- scegli ruolo --','',array('orderby'=>'nome'));
        unset($ruoli[1]);
        unset($ruoli[2]);
        $form->select('ruolo','Ruolo associato',$ruoli,$this->config['ruolo']);
        $form->radio('mask','Autenticazione diretta',array(0=>'NO',1=>'SI'));
        $form->radio('version','Sistema login',array(0=>'Cohesion 1',2=>'Cohesion 2'));
    }
    protected function editPost(){/*vuoto*/}
    
    /*METODI PER SUPPORTO AL TEMPLATE*/
    function schema(){
        $objC=new CB_Credenziali;
        $objP=new AV_Cittadino;
        $t1 = array('s'=>array('CMS_DIR','URL_LOGIN'));
        $t2 = array('s'=>array('URL_LOGOUT',$objC,$objP));
        return array('non_loggato'=>$t1,'loggato'=>$t2);
    }
    
    function defaultTemplate(){
        $loggato='<div>Benvenuto <b>[CB_Credenziali.username]</b></div>
<form action="[URL_LOGOUT]" method="post">
<div class="formBody">
<div class="azioni">
<input value="Logout" class="invia_form" id="submit1" name="submit1" type="submit">
</div>
</div>
</form>';
        
        $nonLoggato='<div class="auth">
<img src="[CMS_DIR]/img/lock.gif" alt="img" title="img">
<a href="[URL_LOGIN]">Autenticati</a>
</div>
<a href="http://registrazione.regione.marche.it/RegistrazioneCohesion/" id="linkCohesionReg">Modulo di registrazione</a>
';
        return array('non_loggato'=>$nonLoggato,'loggato'=>$loggato);
    }
}

?>