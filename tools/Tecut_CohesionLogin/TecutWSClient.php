<?php
class TecutWSClient extends AV_SoapClient{

    function setHeader($conf=array()){
        $s = '';
        if($conf){
            if($conf['mitt']) $s .= "<mittente>$conf[mitt]</mittente>";
            if($conf['dest']) $s .= "<destinatario>$conf[dest]</destinatario>";
            if($conf['cohesion']) $s .= "<autenticazione><IdSessioneSSO>valxx</IdSessioneSSO><IdSessioneASPNET>valxx</IdSessioneASPNET></autenticazione>";
            if($conf['semplice']) $s .= "<autenticazione>andrea</autenticazione>";
        }
        $this->__setHeader('content',$s);
    }

    function unsetHeader(){
        $this->header='';
    }
}
?>
