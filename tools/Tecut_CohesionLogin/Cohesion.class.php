<?php
/**
 * Classe per la gestione del login di Cohesion
 * 
 * @version 2.1.0 22/11/2013 18.17
 * @author Andrea Vallorani
 */
class Cohesion{
    
    const COHESION_SSO = 'http://cohesion.regione.marche.it/SA/LoginFrame.aspx?auth=';
    const TECUT_SSO = 'http://ws.tecut2.it/ws/cohesion/cohesion.php?wsdl';
    
    function __construct($dominio,$urlPagina){
        $this->dominio = str_replace('&','&amp;',$dominio);
        $this->urlPagina = str_replace('&','&amp;',$urlPagina);
    }
    
    function login(){
        $urlPagina = str_replace('&amp;','&',$this->urlPagina);
        $urlValidate = AV_HTTPClient::getServerUrl().'/tools/Tecut_CohesionLogin/validate2.php?cms='.urlencode(base64_encode($urlPagina));
        $xmlAuth='<dsAuth xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://tempuri.org/Auth.xsd">
            <auth>
                <user />
                <id_sa />
                <id_sito>SITRA</id_sito>
                <esito_auth_sa />
                <id_sessione_sa />
                <id_sessione_aspnet_sa />
                <url_validate>'.$urlValidate.'</url_validate>
                <url_richiesta>'.$this->urlPagina.'</url_richiesta>
                <esito_auth_sso>NO</esito_auth_sso>
                <id_sessione_sso />
                <id_sessione_aspnet_sso />
                <stilesheet>main.css</stilesheet>
            </auth>
        </dsAuth>';
        $auth = urlencode(base64_encode($xmlAuth));
        $urlLogin=self::COHESION_SSO.$auth;
        header("Location: $urlLogin");
        exit;
    }
    
    function check($auth){
        $xml=base64_decode($auth);
        $domXML = new domDocument;
        $domXML->loadXML($xml);
        $esito=$domXML->getElementsByTagName('esito_auth_sso')->item(0)->nodeValue;
        if($esito!='OK') throw new Exception('Login failed');
        $this->sso = $domXML->getElementsByTagName('id_sessione_sso')->item(0)->nodeValue;
        $this->aspnet = $domXML->getElementsByTagName('id_sessione_aspnet_sso')->item(0)->nodeValue;
        $this->username = $domXML->getElementsByTagName('user')->item(0)->nodeValue;
    }
    
    function getProfile(){
        $tecut = new TecutWSClient(self::TECUT_SSO);
        $tecut->setHeader(array('mitt'=>$this->dominio,'semplice'=>1));
        $this->profile = $tecut->getProfile($this->sso,$this->aspnet);
        return $this->profile;
    }
}

?>
