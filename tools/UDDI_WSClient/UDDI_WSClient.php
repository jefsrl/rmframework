<?php
/***************************************************
                UDDI WSClient
 v: 1.0.2
 d: 14/01/2015 17.59
 c: UDDI_WSClient
 g: UDDI
 t: Blocco per interrogazione servizi del catalogo UDDI
 a: Andrea Vallorani
****************************************************/
class UDDI_WSClient extends CB_WSClient{
    
    protected $campiConfig = array('wsdl','method','logphp','model','print','download','endpoint');
    protected $campiFormat = array();
    
    public function inizializza(){
        $this->setConf('print',1);
        $this->update(1);
    }
    
    protected function costruisciBlocco(){
        if($this->config['wsdl'] && !$this->config['method']){
            $menu=$this->body->addDivC('menu');
            $this->addJS(CMS::jquery);
            if($this->config['download']){
                $ext=strtoupper(AV_File::ext($this->config['wsdl']));
                $menu->add(new AV_Link('<img src="'.CMS::siteDir().'/tools/UDDI_WSClient/img/iodlE.png" title="Licenza CC" style="height:50px"/>','http://www.dati.gov.it/iodl/2.0/',array('class'=>'blank')));
                $menu->aCapo(2);
                $menu->add(new AV_Link('<img src="'.CMS::siteDir().'/img/root/database.png" /> Formato '.$ext,$this->config['wsdl'],array('class'=>'xml blank')));
                $menu->aCapo();
                $menu->add('Url fonte dato: '.$this->config['wsdl'],1);
                $menu->add('Ultimo aggiornamento: 22/11/2013 12:34',1);
                $menu->add('Proprietario: Regione Marche',1);
                $menu->add('Licenza: ');
                $menu->add(new AV_Link('Leggi il contratto','http://www.dati.gov.it/iodl/2.0/',array('class'=>'blank','style'=>'color:#222')));
            }
            return;
        }
        if(!$this->inModifica && !empty($_POST['wsgo'])){
            $this->specialF($_POST['p']);
            $menu=$this->body->addDivC('menu');
            $this->addJS(CMS::jquery);
            $this->addJS('print.js',1);
            if($this->config['print']) $menu->add(new AV_Link('Stampa certificato',CMS::newUrl('uddi','stampa',$this->conf->titolo),array('class'=>'pdf blank','title'=>$this->conf->titolo)));
            if($this->config['download']){
                $menu->add(new AV_Link('Formato XML','#',array('class'=>'xml')));
                $menu->add(new AV_Link('Formato CSV','#',array('class'=>'csv')));
                $menu->add(new AV_Link('<img src="'.CMS::siteDir().'/tools/UDDI_WSClient/img/iodlE.png" title="Licenza CC" style="height:50px"/>','http://www.dati.gov.it/iodl/2.0/',array('class'=>'blank')));
            }
        }
        elseif(!$this->inModifica && empty($_POST['wsgo']) && $this->getTemplate('request')==''){
            $_POST['wsgo'] = 1;
        }
        parent::costruisciBlocco();
    }
    
    private function specialF(&$array){
        foreach($array as $k=>$v){
            if(is_array($v)) $array[$k]=$this->specialF($v);
            else{
                switch($v){
                    case '__cf':
                        $c=new AV_Cittadino(User::get('profilo'));
                        $array[$k]=$c->codiceFiscale;
                    break;
                    case '__istat':
                        $ente=$this->getEnte();
                        if($ente) $array[$k]=$ente->istat;
                    break;
                    case '__ssoid':
                        $array[$k] = User::space('cohesion_sso');
                    break;
                    case '__aspnetid':
                        $array[$k] = User::space('cohesion_aspnet');
                    break;
                }
            }
        }
        return $array;
    }
    
    private function getEnte(){
        $pag=new CB_File($_GET['p']);
        while($pag->id){
            $ente=UDDI_Ente::first(array('pag'=>$pag->id));
            if($ente) break;
            $pag=$pag->obj('padre');
        }
        return $ente;
    }    
    /**
     * 
     * @param AV_Form2 $form
     */
    protected function formImpostazioni($form){
        $ente=$this->getEnte();
        if($ente){
            $servizi=UDDI_Servizio::find(array('ente'=>$ente->id),array('orderby'=>'nome'));
            if($servizi){
                $serviziExp=array();
                foreach($servizi as $obj){
                    if($obj->isWS()){
                        $ws=new AV_SoapClient($obj->wsdl);
                        $metodi=$ws->__getFunctionsNames();
                        foreach($metodi as $metodo){
                            if($obj->tipo==1){
                                $serviziExp[$obj->wsdl.'|'.$metodo.'|'.$obj->endpoint]=$obj->nome.'->'.$metodo;
                            }
                            else{
                                $serviziExp[$obj->wsdl.'|'.$metodo.'|'.$obj->endpoint.'/'.$metodo]=$obj->nome.'->'.$metodo;
                            }
                        }
                    }
                    else{
                        $serviziExp[$obj->endpoint.'||']=$obj->nome;
                    }
                }
                $form->select('wsdl','Servizio',$serviziExp,$this->config['wsdl'].'|'.$this->config['method'].'|'.$this->config['endpoint']);
                $modelli=AV_Find::getElenco('UDDI_Modello','id','nome','- no -',array(),array('orderby'=>'nome'));
                $form->select('model','Modello predefinito',$modelli);
                $form->radio('logphp','Stampa log',array(0=>'NO',1=>'SI'));
                $form->radio('print','Stampa certificato',array(0=>'NO',1=>'SI'));
                $form->radio('download','Scarica risultato',array(0=>'NO',1=>'SI'));
            }
            else{
                $form->add('Nessun servizio caricato nel registro per l\'ente '.$ente->nome);
            }
        }
        else{
            $form->add('Nessun ente trovato');
        }
    }
    
    function configUpdate($p){
        list($wsdl,$metodo,$endpoint)=explode('|',$_POST[$this->sigla.'wsdl']);
        $_POST[$this->sigla.'wsdl']=$wsdl;
        $_POST[$this->sigla.'method']=$metodo;
        $_POST[$this->sigla.'endpoint']=$endpoint;
        parent::configUpdate($p);
    }
    
    function defaultTemplate(){
        $templates=parent::defaultTemplate();
        if(!empty($this->config['model'])){
            $obj=new UDDI_Modello($this->config['model']);
            $templates['request']=$obj->request;
            $templates['response']=$obj->response;
        }
        return $templates;
    }
    
    static function pdf64download($name,$val){
        AV_HTTPClient::downloadContent($name,base64_decode($val),'application/pdf');
    }
}