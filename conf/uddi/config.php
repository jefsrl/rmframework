<?php
/**
 * Gestore configurazioni uddi
 * 
 * @version 1.0.0 09/12/2014 17.56
 * @author Andrea Vallorani 
 */
class Ext_UDDI extends Ext_base{
    
    const NAME='UDDI';
    const VERSION='1.0.0';
    const COD='uddi';
    const OBJCOD='UDDI';
    const VARCOD='uddi';
    const CMS_MIN='1.1.8';
    const CMS_MAX='1.1.1000';
    
    //funzione di configurazione
    function extension(){
        if($_POST){
            Config::setAll('uddi',$_POST);
            return 1;
        }
        else{
            $form = new AV_Form2();
            $default=Config::getAll('uddi');
            $form->addDefaultValues($default);
            $form->attr('action',CMS::getUrl());

            /*Generali*/
            //$form->group('uddi_gen','Registro UDDI');
            $form->text('uddi_url','Indirizzo','',250,50);
            $form->text('uddi_usr','Utente','',100,30);
            $form->text('uddi_pwd','Password','',100,30);
            $form->add('<label>Report registro</label>');
            $form->add(new AV_Link('UDDI Browser',CMS::newUrl('uddi','browser','','','rewrite'),array('target'=>'_blank')),1);
            //$form->add(' | ',1);
            //$form->add(new AV_Link('Elenco servizi',CMS::newUrl('uddi','find_service'),array('target'=>'_blank')),1);
            
            return $form;
       }
    }
}